const subscriberModel = require('../models/subscriber.model');
const winston = require('../configs/winston.config');

const attributes = [
  'msisdn',
  'surname',
  'documentType',
  'countryOfIssue',
  'dateOfIssue',
  'seriesAndNumber',
  'divisionCode',
  'issuingAuthority',
  'registrationAddress'
];

const subscriberService = {

  createTable: function () {
    return subscriberModel.sync({force: true})
      .catch((err) => {
        winston.error('err in creating table of subscriber:', err);
      })
  },

  addSubscriber: function (subscriber) {
    return subscriberModel.create(subscriber)
      .catch((err) => {
        return 'err in adding subscriber'
      });
  },

  getSubscriberByMsisdn: function (msisdn) {
    return subscriberModel.findOne({
      where: {msisdn: msisdn},
      attributes: attributes
    })
      .catch((err) => {
        return 'with this msisdn subscribers did not found'
      });
  },

  deleteSubscriberByMsisdn: function (msisdn) {
    return subscriberModel.destroy({
      where: {msisdn: msisdn}
    })
      .catch((err) => {
        return 'with this msisdn subscribers did not found'
      });
  },

  updateSubscriberByMsisdn: function (msisdn, subscriber) {
    return subscriberModel.update(subscriber, {
      where: {msisdn: msisdn}
    })
      .then(() => {
        return subscriberModel.findOne({
          where: {msisdn: msisdn},
          attributes: attributes
        })
      })
      .catch((err) => {
        return 'with this msisdn subscribers did not found'
      });
  },

  getSubscriberList: function () {
    return subscriberModel.findAll({
      attributes: ['msisdn', 'surname', 'dateOfIssue']
    })
  }

};

module.exports = subscriberService;
