const winston = require('../configs/winston.config');

module.exports = {

  isError: function (validationErr, failingFunction, successFunction) {
    !validationErr.isEmpty() ? failingFunction(validationErr.array()[0].msg) : successFunction();
  },

  ifError: function (error, nextFun) {
    winston.info(error);
    nextFun(error);
  }
}
