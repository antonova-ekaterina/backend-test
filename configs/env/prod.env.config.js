let prodConfig = {
  server:{
    port: 5000,
    host: '127.0.0.1'
  },
  db: {
    port: 5430,
    host: '127.0.0.1',
    name: 'test',
    userName: 'postgres',
    password: '1'
  },
  morgan: {
    format: (tokens, req, res) => {
      return [
        tokens.method(req, res),
        tokens.url(req, res),
        tokens.status(req, res)
      ].join(' ')
    }
  },
  winston: {
    loggingLevel: 'info'
  },
};

module.exports = prodConfig;