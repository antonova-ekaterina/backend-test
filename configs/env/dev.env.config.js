let devConfig={
  server:{
    port: 4000,
    host: '0.0.0.0'
  },
  db: {
    port: 5432,
    host: '127.0.0.1',
    name: 'test-1',
    userName: 'postgres',
    password: '1'
  },
  morgan: {
    format: 'dev'
  },
  winston: {
    loggingLevel: 'debug'
  }
};

module.exports = devConfig;