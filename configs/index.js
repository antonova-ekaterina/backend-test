let devConfig = require('./env/dev.env.config');
let prodConfig = require('./env/prod.env.config');

function setEnvConfig() {
  switch (process.env.NODE_ENV) {
    case 'dev':
      return devConfig;
    case 'prod':
      return prodConfig;
    default :
      return devConfig
  }
}

module.exports = setEnvConfig();