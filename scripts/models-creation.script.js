const sequelize = require('../configs/sequelize.config')
  , subscriberService = require('../services/subscriber.service');

sequelize.connect()
  .then(() => {
    return subscriberService.createTable();
  })
  .then(() => {
    const subscriber = {
      surname: 'Васильев',
      documentType: 'Паспорт',
      countryOfIssue: 'Россия',
      dateOfIssue: Date.now(),
      seriesAndNumber: '1234123456',
      divisionCode: '1234567890',
      issuingAuthority: 'ОУФМС 399394',
      registrationAddress: 'ул. Южная 25'
    };
    subscriberService.addSubscriber(subscriber);
  });
