const morgan = require('morgan');
const config = require('../configs');
const router = require('express').Router();

router.use(morgan(config.morgan.format, {
  skip: (req, res) => {
    return res.statusCode < 400
  },
  stream: process.stderr
}));

router.use(morgan(config.morgan.format, {
  skip: (req, res) => {
    return res.statusCode >= 400
  },
  stream: process.stdout
}));

module.exports = router;
