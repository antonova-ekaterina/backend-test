const router = require('express').Router();
const subscriberController = require('../controllers/subscriber.controller');
const {check} = require('express-validator/check');

router.post('/', [
    check('surname', 'invalid surname').not().isEmpty(),
    check('documentType', 'invalid documentType').not().isEmpty(),
    check('countryOfIssue', 'invalid countryOfIssue').not().isEmpty(),
    check('dateOfIssue', 'invalid dateOfIssue').not().isEmpty(),
    check('seriesAndNumber', 'invalid seriesAndNumber').not().isEmpty(),
    check('divisionCode', 'invalid divisionCode').not().isEmpty(),
    check('issuingAuthority', 'invalid issuingAuthority').not().isEmpty(),
    check('registrationAddress', 'invalid registrationAddress').not().isEmpty(),
  ],
  subscriberController.createSubscriber
);

router.get('/:msisdn', subscriberController.getSubscriber);

router.put('/:msisdn', [
    check('surname', 'invalid surname').not().isEmpty(),
    check('documentType', 'invalid documentType').not().isEmpty(),
    check('countryOfIssue', 'invalid countryOfIssue').not().isEmpty(),
    check('dateOfIssue', 'invalid dateOfIssue').not().isEmpty(),
    check('seriesAndNumber', 'invalid seriesAndNumber').not().isEmpty(),
    check('divisionCode', 'invalid divisionCode').not().isEmpty(),
    check('issuingAuthority', 'invalid issuingAuthority').not().isEmpty(),
    check('registrationAddress', 'invalid registrationAddress').not().isEmpty(),
  ], subscriberController.changeSubscriber
);

router.delete('/:msisdn', subscriberController.deleteSubscriber);

router.get('/list', subscriberController.getList);

module.exports = router;
