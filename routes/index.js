const morgan = require('./morgan.routes');
const subscriberRoutes = require('./subscriber.route');
const router = require('express').Router();

router.use(morgan);
router.use('/subscriber', subscriberRoutes);

module.exports = router;
