const subscriberService = require('../services/subscriber.service');
const validationService = require('../services/validation.service');
const {validationResult} = require('express-validator/check');

const subscriberController = {

  createSubscriber: (req, res, next) => {
    validationService.isError(validationResult(req), next, () => {
      const newSubscriber = {
        surname: req.body.surname,
        documentType: req.body.documentType,
        countryOfIssue: req.body.countryOfIssue,
        dateOfIssue: req.body.dateOfIssue,
        seriesAndNumber: req.body.seriesAndNumber,
        divisionCode: req.body.divisionCode,
        issuingAuthority: req.body.issuingAuthority,
        registrationAddress: req.body.registrationAddress
      };
      subscriberService.addSubscriber(newSubscriber)
        .then((subscriber) => {
          req.dataOut = subscriber;
          next();
        })
        .catch((error) => {
          validationService.ifError(error, next);
        });
    });
  },

  getSubscriber: (req, res, next) => {
    subscriberService.getSubscriberByMsisdn(req.params.msisdn)
      .then((subscriber) => {
        req.dataOut = subscriber;
        next();
      })
      .catch((error) => {
        validationService.ifError(error, next);
      });
  },

  changeSubscriber: (req, res, next) => {
    validationService.isError(validationResult(req), next, () => {
      const newVale = {
        surname: req.body.surname,
        documentType: req.body.documentType,
        countryOfIssue: req.body.countryOfIssue,
        dateOfIssue: req.body.dateOfIssue,
        seriesAndNumber: req.body.seriesAndNumber,
        divisionCode: req.body.divisionCode,
        issuingAuthority: req.body.issuingAuthority,
        registrationAddress: req.body.registrationAddress
      };
      subscriberService.updateSubscriberByMsisdn(req.params.msisdn, newVale)
        .then((subscriber) => {
          req.dataOut = subscriber;
          next();
        })
        .catch((error) => {
          validationService.ifError(error, next);
        });
    });

  },

  deleteSubscriber: (req, res, next) => {
    subscriberService.deleteSubscriberByMsisdn(req.params.msisdn)
      .then(() => {
        req.dataOut = 'deleted';
        next();
      })
      .catch((error) => {
        validationService.ifError(error, next);
      });
  },

  getList: (req, res, next) => {
    subscriberService.getSubscriberList()
      .then((subscribers) => {
        req.dataOut = subscribers;
        next();
      })
      .catch((error) => {
        validationService.ifError(error, next);
      });
  }
};

module.exports = subscriberController;
