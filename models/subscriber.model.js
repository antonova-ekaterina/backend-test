const sequelize = require('../configs/sequelize.config').connection;
const Sequelize = require('sequelize');

const Subscriber = sequelize.define('subscriber', {
  //ToDO fix msisdn value
  msisdn: {
    type: Sequelize.UUID,
    primaryKey: true,
    defaultValue: Sequelize.UUIDV4
  },
  surname: {type: Sequelize.STRING, allowNull: false},
  documentType: {
    type: Sequelize.STRING,
    allowNull: false,
    values: ['Паспорт' | 'Загран паспорт' | 'Паспорт моряка']
  },
  countryOfIssue: {type: Sequelize.STRING, allowNull: false},
  dateOfIssue: {type: Sequelize.DATE, allowNull: false},
  seriesAndNumber: {type: Sequelize.STRING, allowNull: false},
  divisionCode: {type: Sequelize.STRING, allowNull: false},
  issuingAuthority: {type: Sequelize.STRING, allowNull: false},
  registrationAddress: {type: Sequelize.STRING, allowNull: false},
});
module.exports = Subscriber;